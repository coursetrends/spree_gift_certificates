module Spree
  module Admin
    class GiftCertificatesController < ResourceController
      before_filter :load_data

      def update
        respond_with @gift_certificate.update!(gift_certificate_params)
      end

      def gift_certificate_params
        params.require(':gift_certificate').permit(:enabled, :item_name, :special_instructions, :expires)
      end

    end
  end
end
