module Spree
  DigitalsController.class_eval do
    alias_method :original_show, :show
    
    def show

      if is_gift_certificate?
        
        if digital_link.authorize!
          pdf_creator = CreateGiftCertificatePdf.new
          send_data pdf_creator.generate_pdf(get_product.gift_certificate, get_order, get_certificate_number)
        else
          render :unauthorized
        end
      else
        original_show
      end
    end
    
    private

    def is_gift_certificate?
     get_product.gift_certificate.enabled 
    end

    def get_product
      digital_link.digital.variant.product
    end

    def get_order
      digital_link.line_item.order
    end
   
    def get_certificate_number
      "#{digital_link.line_item.order.id}-#{digital_link.id}"
    end
    
  end
end
