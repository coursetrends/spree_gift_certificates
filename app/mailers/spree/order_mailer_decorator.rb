module Spree
  OrderMailer.class_eval do
    alias_method :original_confirm_email, :confirm_email

    def confirm_email(order, resend = false)
      order = order.respond_to?(:id) ? order : Spree::Order.find(order)

      attachment_count = 0
      order.line_items.each do |line_item|
        line_item.digital_links.each do |digital_link|
          if is_gift_certificate?(digital_link)
            attachment_count = attachment_count + 1
            pdf_creator = CreateGiftCertificatePdf.new
            attachments["gift_certificates_#{attachment_count}.pdf"] = { 'Content-type' => 'application/pdf',
                  :content => pdf_creator.generate_pdf(get_product(digital_link).gift_certificate, order, get_certificate_number(digital_link))}
          end
        end
      end

      original_confirm_email(order, resend)
    end

    private

    def is_gift_certificate?(digital_link)
     get_product(digital_link).gift_certificate.enabled
    end

    def get_product(digital_link)
      digital_link.digital.variant.product
    end

    def get_certificate_number(digital_link)
      "#{digital_link.line_item.order.id}-#{digital_link.id}"
    end

  end
end
