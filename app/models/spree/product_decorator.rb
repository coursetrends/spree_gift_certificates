module Spree
  Product.class_eval do
    has_one :gift_certificate
    accepts_nested_attributes_for :gift_certificate
    after_create :create_gift_certificate_if_gift_certificate_is_nil
    after_save :add_or_destroy_digital_based_on_gift_certificate

    def has_gift_certificate
      self.gift_certificate.enabled
    end

    private

    def create_gift_certificate_if_gift_certificate_is_nil
      return unless self.gift_certificate.nil?
      gift_certificate = Spree::GiftCertificate.create
      self.gift_certificate = gift_certificate
      self.save
    end

    def add_or_destroy_digital_based_on_gift_certificate
      if self.gift_certificate.enabled
        create_digitals_and_set_gift_certificate_settings
      else
        destroy_digitals_and_unset_gift_certificate_settings
      end
    end

    def create_digitals_and_set_gift_certificate_settings
      digital =  Spree::Digital.create(attachment: File.new(File.join(File.dirname(File.expand_path(__FILE__)), '../../assets/pdfs/gift_certificate.pdf')))
      self.shipping_category = Spree::ShippingCategory.find_by_name('Digital Delivery Category')
      self.variants_including_master.each do |variant|
        variant.digitals << digital
        variant.track_inventory = false
        variant.save
      end
    end

    def destroy_digitals_and_unset_gift_certificate_settings
      self.variants_including_master.each do |variant|
        variant.digitals.destroy_all
      end
    end

  end
end
