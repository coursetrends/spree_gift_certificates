Deface::Override.new(:virtual_path => 'spree/admin/products/_form',
                     :name => 'add_gift_certificate_to_product_edit',
                     :insert_after => "div[data-hook='admin_product_form_description']",
                     :text => "<script type=text/javascript>
function showHideGiftCertificateFields() {
  if(document.getElementById('product_gift_certificate_attributes_enabled').checked) {
    $('.gift_certificate_field').show();
  } else {
    $('.gift_certificate_field').hide();
  }
}
$(document).ready(showHideGiftCertificateFields);
</script>
<div data-hook='admin_product_form_gift_certificate' class='col-md-8'>
<%= f.fields_for :gift_certificate do |gift_certificate_fields| %>
   <%= f.field_container :enabled do %>
     <%= gift_certificate_fields.check_box :enabled, :checked => @product.gift_certificate.enabled, :onchange => 'showHideGiftCertificateFields()' %>
     <%= gift_certificate_fields.label :enabled, raw(Spree.t(:gift_certificate)) %>
   <% end %>
   <%= f.field_container :item_name, class: ['gift_certificate_field', 'col-md-6'] do %>
     <%= gift_certificate_fields.label :item_name, raw(Spree.t(:item_name)) %>
     <%= gift_certificate_fields.text_field :item_name, :value => @product.gift_certificate.item_name %>
   <% end %>
   <%= f.field_container :special_instructions, class: ['gift_certificate_field', 'col-md-6'] do %>
     <%= gift_certificate_fields.label :special_instructions, raw(Spree.t(:special_instructions)) %>
     <%= gift_certificate_fields.text_field :special_instructions, :value => @product.gift_certificate.special_instructions %>
   <% end %>
   <%= f.field_container :expires, class: ['gift_certificate_field', 'col-md-6'] do %>
     <%= gift_certificate_fields.label :expires, raw(Spree.t(:expires)) %>
     <%= gift_certificate_fields.text_field :expires, :value => @product.gift_certificate.expires %>
   <% end %>
   </div>
<% end %>
</div>
")
