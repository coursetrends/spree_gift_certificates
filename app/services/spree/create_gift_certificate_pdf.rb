require 'prawn'
require 'barby'
require 'barby/barcode/code_39'
require 'barby/outputter/png_outputter'

module Spree
  class CreateGiftCertificatePdf

    LEFT_MARGIN = 35
    TOP_HEADER_HEIGHT = 60
    RIGHT_BOX_LEFT_MARGIN = 360
    RIGHT_BOX_TOP_MARGIN = 45
    BACKGROUND_IMAGE = File.join(File.dirname(File.expand_path(__FILE__)), '../../assets/images/gift_certificate.png')
    DEFAULT_MAIN_IMAGE = File.join(File.dirname(File.expand_path(__FILE__)), '../../assets/images/spacer.png')

    def generate_pdf(gift_certificate, order, certificate_number)
      pdf= Prawn::Document.new
      pdf.image BACKGROUND_IMAGE, :width => pdf.bounds.width
      pdf.bounding_box([LEFT_MARGIN, pdf.bounds.height - TOP_HEADER_HEIGHT], :width => 300, :height => 200) do
        pdf.stroke_horizontal_rule
        pdf.move_down 5
        pdf.font('Times-Bold')
        pdf.font_size(12)
        pdf.text "Date of Issue: #{order.completed_at.strftime('%b %e, %Y')}"

        pdf.move_down 20
        pdf.font('Times-Roman')
        pdf.font_size(11)
        pdf.text "This certificate entitles: Bearer"
        pdf.stroke_horizontal_rule

        pdf.move_down 15
        pdf.text "To: #{gift_certificate.item_name}"
        pdf.stroke_horizontal_rule

        pdf.move_down 15
        pdf.text "Certificate #: #{certificate_number}"
        pdf.stroke_horizontal_rule

        pdf.move_down 5
        pdf.font_size(8)
        pdf.text "Purchased By: #{order.bill_address.firstname} #{order.bill_address.lastname}\n"
        pdf.text "Expires #{gift_certificate.expires}"

      end

      pdf.bounding_box([RIGHT_BOX_LEFT_MARGIN, pdf.bounds.height - RIGHT_BOX_TOP_MARGIN], :width => 150, :height => 200) do
        begin
            main_image_url = "http://#{order.store.url}/images/design/logo.png"
            main_image = open(main_image_url)
        rescue => e
            main_image = open(DEFAULT_MAIN_IMAGE) if !main_image
        end
        pdf.image main_image, :fit => [125, 75]

        pdf.font('Times-Roman')
        pdf.font_size(8)
        pdf.text gift_certificate.special_instructions

        pdf.move_down 20

        barcode = Barby::Code39.new(certificate_number, true)
        blob = barcode.to_png({height: 20, margin: 0})
        data = StringIO.new(blob)
        pdf.image data, :position => :center
        pdf.move_down 3
        pdf.font('Courier-Bold')
        pdf.font_size(8)
        pdf.text certificate_number, :align => :center
      end

      pdf.render
    end

  end
end

