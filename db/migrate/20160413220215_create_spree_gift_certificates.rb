class CreateSpreeGiftCertificates < ActiveRecord::Migration
  def change

    create_table :spree_gift_certificates do |t|
      t.integer :product_id
      t.boolean :enabled
      t.string :item_name
      t.string :special_instructions
      t.string :expires
    end
  end
end
