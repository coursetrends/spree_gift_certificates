# encoding: UTF-8
Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'spree_gift_certificates'
  s.version     = '3.1.0'
  s.summary     = 'Downloadable Gift Certificates for Spree eCommerce'
  s.description = 'Downloadable Gift Certificates for Spree eCommerce'
  s.required_ruby_version = '>= 2.0.0'

  s.author    = 'Clifford Butler'
  s.email     = 'cbutler@distinctsoftwaresolutions.com'
  # s.homepage  = 'http://www.spreecommerce.com'

  #s.files       = `git ls-files`.split("\n")
  #s.test_files  = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'spree_core', '~> 3.1.0.rc1'
  s.add_dependency 'prawn', '~> 2.1.0'
  s.add_dependency 'barby', '~> 0.6.2'
  s.add_dependency 'chunky_png', '~> 1.3.5'

  s.add_development_dependency 'capybara', '~> 2.4'
  s.add_development_dependency 'coffee-rails'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_girl', '~> 4.5'
  s.add_development_dependency 'ffaker'
  s.add_development_dependency 'rspec-rails',  '~> 3.1'
  s.add_development_dependency 'sass-rails', '~> 5.0.0.beta1'
  s.add_development_dependency 'selenium-webdriver'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'sqlite3'
end
